package ro.tuc.ds2020.model;

import java.util.List;

public class MedicationPlanForToday {
    private List<MedicationListItemDetailsDTO> medicationListItem;
    private String  periodOfTheTreatment;

    public MedicationPlanForToday(List<MedicationListItemDetailsDTO> medicationListItem, String periodOfTheTreatment) {
        this.medicationListItem = medicationListItem;
        this.periodOfTheTreatment = periodOfTheTreatment;
    }

    public MedicationPlanForToday() {
    }

    public List<MedicationListItemDetailsDTO> getMedicationListItem() {
        return medicationListItem;
    }

    public void setMedicationListItem(List<MedicationListItemDetailsDTO> medicationListItem) {
        this.medicationListItem = medicationListItem;
    }

    public String getPeriodOfTheTreatment() {
        return periodOfTheTreatment;
    }

    public void setPeriodOfTheTreatment(String periodOfTheTreatment) {
        this.periodOfTheTreatment = periodOfTheTreatment;
    }

    @Override
    public String toString() {
        return "MedicationPlanForToday{" +
                "medicationListItem=" + medicationListItem +
                ", periodOfTheTreatment='" + periodOfTheTreatment + '\'' +
                '}';
    }
}
