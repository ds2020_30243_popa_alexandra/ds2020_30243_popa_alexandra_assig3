package ro.tuc.ds2020.model;

import java.util.UUID;

public class MedicationListItemDetailsDTO {
    private UUID id;
    private  String  intakeIntervals;
    private MedicationDTO medication;
    private Boolean medicationTaken;

    public MedicationListItemDetailsDTO(UUID id, String intakeIntervals, MedicationDTO medication) {
        this.id = id;
        this.intakeIntervals = intakeIntervals;
        this.medication = medication;
        this.medicationTaken=false;
    }

    public MedicationListItemDetailsDTO() {
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getIntakeIntervals() {
        return intakeIntervals;
    }

    public void setIntakeIntervals(String intakeIntervals) {
        this.intakeIntervals = intakeIntervals;
    }

    public MedicationDTO getMedication() {
        return medication;
    }

    public void setMedication(MedicationDTO medication) {
        this.medication = medication;
    }

    public Boolean getMedicationTaken() {
        return medicationTaken;
    }

    public void setMedicationTaken(Boolean medicationTaken) {
        this.medicationTaken = medicationTaken;
    }
}
