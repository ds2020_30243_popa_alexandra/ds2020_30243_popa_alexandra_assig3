package ro.tuc.ds2020.GUI;


import ro.tuc.ds2020.Service.ClientInterface;
import ro.tuc.ds2020.model.MedicationListItemDetailsDTO;
import ro.tuc.ds2020.model.MedicationPlanForToday;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;


public class TakenButton extends JButton implements TableCellRenderer {
    private JButton button;
    private MedicationPlanForToday medPlan;
    private  ClientInterface rmi;
    public TakenButton() {

        setOpaque(true);
        button=new JButton();


    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column)  {
        if (isSelected) {

            DefaultTableModel model=(DefaultTableModel) table.getModel();
            if(column !=0) {
                for (MedicationListItemDetailsDTO med : medPlan.getMedicationListItem()) {
                    if (med.getMedication().getName().equals(table.getValueAt(row, 1)) &&
                            med.getIntakeIntervals().equals(table.getValueAt(row, 0)) &&
                            med.getMedication().getDosage().equals(table.getValueAt(row, 2)) &&
                            med.getMedication().getSideEffects().equals(table.getValueAt(row, 3))) {

                        med.setMedicationTaken(true);
                        rmi.sendNotification(med);
                    }
                }

                model.removeRow(row);


            }
        } else {
            setForeground(table.getForeground());
            setBackground(UIManager.getColor("Button.background"));
        }
        setText((value == null) ? "" : value.toString());
        return this;
    }

    public void setMedPlan(MedicationPlanForToday medPlan) {
        this.medPlan = medPlan;
    }


    public  void setRmi(ClientInterface rmi) {
        this.rmi = rmi;
    }

    public JButton getButton() {
        return button;
    }
}
