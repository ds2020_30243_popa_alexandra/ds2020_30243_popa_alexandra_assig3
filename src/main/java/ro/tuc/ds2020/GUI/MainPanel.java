package ro.tuc.ds2020.GUI;

import ro.tuc.ds2020.Service.ClientInterface;

import ro.tuc.ds2020.model.MedicationListItemDetailsDTO;
import ro.tuc.ds2020.model.MedicationPlanForToday;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainPanel extends JPanel {

    private JLabel medPlanLabel;
    private JTable medTable;
    private DefaultTableModel model;
    private ClientInterface rmi;
    private TakenButton button;
    private TimerTask task1;
    private TimerTask task2;
    Timer timer = new Timer();


    public MainPanel() {


        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        JPanel timePanel = new JPanel();


        medTable = new JTable();
        medTable.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);


        medTable.setFillsViewportHeight(true);

        add(medTable);


        JScrollPane scroll = new JScrollPane(medTable);

        scroll.setColumnHeaderView(medTable.getTableHeader());
        add(scroll);

        final JLabel timeLabel = new JLabel();
        task1 = new TimerTask() {
            @Override
            public void run() {

                SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");

                Date date = new Date();
                String time = format.format(date);
                timeLabel.setText(time);

                timePanel.add(timeLabel);
                add(timePanel);

                medPlanLabel = new JLabel();
                add(medPlanLabel);


                String StringCurrentTime = null;
                Date currentTime = null;
                Date downloadTime = null;

                try {
                    downloadTime = format.parse("16:42:00");
                    StringCurrentTime = format.format(date);
                    currentTime = format.parse(StringCurrentTime);
                } catch (ParseException parseException) {
                    parseException.printStackTrace();
                }


                Calendar calendar1 = Calendar.getInstance();
                calendar1.setTime(currentTime);
                Calendar calendar2 = Calendar.getInstance();
                calendar2.setTime(downloadTime);
                if (calendar1.getTime().equals(calendar2.getTime())) {

                    addMedicationPlanForToday(rmi.sendMedicationPlanForToday(UUID.fromString("c2b7c1ed-c031-4c0f-8748-8d441ebdce47")));

                }

            }
        };


        timer.scheduleAtFixedRate(task1, 0, 1000);


    }

    public void addMedicationPlanForToday(MedicationPlanForToday plan) {
        if (plan != null) {
            JLabel medPlanLabelPeriodOfTreatment = new JLabel();
            add(medPlanLabelPeriodOfTreatment);
            medPlanLabelPeriodOfTreatment.setText(plan.getPeriodOfTheTreatment());


            task2 = new TimerTask() {
                @Override
                public void run() {



                    if (plan.getMedicationListItem() != null) {


                        model = new DefaultTableModel();
                        model.addColumn("Intake interval");
                        model.addColumn("Name");
                        model.addColumn("Dosage");
                        model.addColumn("Side Effects");
                        model.addColumn("Button");
                        medTable.setModel(model);

                        for (MedicationListItemDetailsDTO med : plan.getMedicationListItem()) {

                            String[] interval = med.getIntakeIntervals().split(" - ");
                            SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
                            String dateString = format.format(new Date());
                            Date startTime = null;
                            Date endTime = null;
                            Date date = new Date();
                            String StringCurrentTime = null;
                            Date currentTime = null;


                            try {
                                startTime = format.parse(interval[0]);
                                endTime = format.parse(interval[1]);
                                StringCurrentTime = format.format(date);
                                currentTime = format.parse(StringCurrentTime);
                            } catch (ParseException parseException) {
                                parseException.printStackTrace();
                            }


                            Calendar calendar1 = Calendar.getInstance();
                            calendar1.setTime(startTime);

                            calendar1.add(Calendar.DATE, 1);
                            Calendar calendar2 = Calendar.getInstance();
                            calendar2.setTime(endTime);
                            calendar2.add(Calendar.DATE, 1);


                            Calendar calendar3 = Calendar.getInstance();
                            calendar3.setTime(currentTime);
                            calendar3.add(Calendar.DATE, 1);



                            if (calendar3.getTime().after(calendar1.getTime()) && calendar3.getTime().before(calendar2.getTime())/* startTime.compareTo(date) * date.compareTo(endTime) >= 0*/ && med.getMedicationTaken() == false) {


                                List<String> list = new ArrayList<String>();
                                list.add(med.getIntakeIntervals());
                                list.add(med.getMedication().getName());
                                list.add(med.getMedication().getDosage());
                                list.add(med.getMedication().getSideEffects());


                                list.add("Taken");

                                model.addRow(list.toArray());


                                button = new TakenButton();
                                button.setMedPlan(plan);
                                button.setRmi(rmi);


                                if (medTable.getColumnModel().getColumnCount() == 5) {
                                    medTable.getColumnModel().getColumn(4).setCellRenderer(button);
                                }

                            } else {
                                if (currentTime.compareTo(Date.from(endTime.toInstant().plusSeconds(5))) == 0 && med.getMedicationTaken() == false) {
                                    rmi.sendNotification(med);
                                }
                            }


                        }
                    }
                }
            };

            timer.scheduleAtFixedRate(task2, 0, 1000);
        }
    }


    public JLabel getMedPlanLabel() {
        return medPlanLabel;
    }

    public void setRmi(ClientInterface rmi) {
        this.rmi = rmi;
    }
}
