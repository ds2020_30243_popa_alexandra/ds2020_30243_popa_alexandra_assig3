package ro.tuc.ds2020;

import ro.tuc.ds2020.GUI.MainPanel;
import ro.tuc.ds2020.Service.ClientInterface;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import ro.tuc.ds2020.model.MedicationPlanForToday;


import javax.swing.*;
import java.awt.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Timer;


@SpringBootApplication
public class DispenserApp extends JFrame {

    private static MainPanel mainPanel;
    private static ClientInterface rmi;
    private static MedicationPlanForToday planForToday;
    public DispenserApp() {


        mainPanel=new MainPanel();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(mainPanel);
        this.setSize(500,500);
        this.setVisible(true);


    }
    public static void main(String[] args) {
        ConfigurableApplicationContext context= SpringApplication.run(DispenserApp.class, args);
        rmi=context.getBean(ClientInterface.class);


        EventQueue.invokeLater(()->{
            try {
                DispenserApp view=context.getBean(DispenserApp.class);
                view.setVisible(true);
            }
            catch (NoSuchElementException e ){

            }

        });
        mainPanel.setRmi(rmi);

    }

}