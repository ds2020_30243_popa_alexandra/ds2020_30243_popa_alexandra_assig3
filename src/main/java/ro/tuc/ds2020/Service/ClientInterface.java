package ro.tuc.ds2020.Service;

import com.googlecode.jsonrpc4j.JsonRpcParam;
import ro.tuc.ds2020.model.MedicationListItemDetailsDTO;
import ro.tuc.ds2020.model.MedicationPlanForToday;

import java.util.UUID;


public interface ClientInterface {
    void sendNotification(@JsonRpcParam(value = "item") MedicationListItemDetailsDTO item) ;
    MedicationPlanForToday sendMedicationPlanForToday(@JsonRpcParam(value = "patientId") UUID patientId) ;

}