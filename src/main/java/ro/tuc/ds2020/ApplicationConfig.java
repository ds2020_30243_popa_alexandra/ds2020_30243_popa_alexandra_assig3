package ro.tuc.ds2020;


import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.googlecode.jsonrpc4j.ProxyUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ro.tuc.ds2020.Service.ClientInterface;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class ApplicationConfig {
    private static final String endpoint = "https://ds2020-spring-popa-alexandra.herokuapp.com/medPlan";
    @Bean
    public JsonRpcHttpClient jsonRpcHttpClient() {
        URL url = null;

        Map<String, String> map = new HashMap<>();
        try {
            url = new URL(ApplicationConfig.endpoint);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return new JsonRpcHttpClient(url, map);
    }

    @Bean
    public ClientInterface exampleClientAPI(JsonRpcHttpClient jsonRpcHttpClient) {
        return ProxyUtil.createClientProxy(getClass().getClassLoader(), ClientInterface.class, jsonRpcHttpClient);
    }
}